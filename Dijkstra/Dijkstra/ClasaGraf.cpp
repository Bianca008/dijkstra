#include "ClasaGraf.h"

void Graf::readList() {
	ifstream f("fisGraf.txt");
	f >> nodes >> edges;
	adjList = new list<nrPairs>[nodes];
	int edgeX, edgeY, cost;

	for (int i = 0; i < edges; i++) {
		f >> edgeX >> edgeY >> cost;
		adjList[edgeX].push_back(make_pair(edgeY, cost));
		adjList[edgeY].push_back(make_pair(edgeX, cost));
	}
	f.close();
}

void printNameCities(int oras) {
	switch (oras) {
	case 0:
		cout << "Alba iulia-> ";
		break;
	case 1:
		cout << "Brasov-> ";
		break;
	case 2:
		cout << "Sighisoara-> ";
		break;
	case 3:
		cout << "Sibiu-> ";
		break;
	case 4:
		cout << "Bucuresti-> ";
		break;
	case 5:
		cout << "Buzau-> ";
		break;
	}
}

void printPath(vector<int> parents, int sosire) {
	if (parents[sosire] == -1)
		return;
	printPath(parents, parents[sosire]);
	printNameCities(sosire);
}

void Graf::Dijkstra() {
	int node, v, distCost;
	priority_queue<nrPairs, vector<nrPairs>, greater<nrPairs>>qu;
	vector<int> pret(nodes, INF);
	vector<int> parents(nodes, -1);

	int plecare, sosire;
	cout << "Please insert the number of the city you wnat to leave: ";
	cin >> plecare;
	cout << "Please insert the number of the city where you want to arrive: ";
	cin >> sosire;

	qu.push(make_pair(0, plecare));
	pret[plecare] = 0;

	while (!qu.empty()) {
		node = qu.top().second;
		qu.pop();
		list<pair<int, int>>::iterator i;
		for (i = adjList[node].begin(); i != adjList[node].end(); i++) {
			v = (*i).first;
			distCost = (*i).second;
			if (pret[v] > pret[node] + distCost) {
				pret[v] = pret[node] + distCost;
				qu.push(make_pair(pret[v], v));
				parents[v] = node;
			}
		}
	}
	printNameCities(plecare);
	printPath(parents, sosire);
	cout << "The lowest price for the tickets is: " << pret[sosire] << "\n";
}