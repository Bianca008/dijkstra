#pragma once

#include<iostream>
#include<fstream>
#include<queue>
#include<list>
using namespace std;

#define INF 99999

typedef pair<int, int> nrPairs;

class Graf {
	int nodes, edges;
	list<pair<int, int>> *adjList;

public:
	void readList();
	void Dijkstra();
};