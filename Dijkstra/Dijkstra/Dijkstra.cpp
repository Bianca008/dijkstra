#include "ClasaGraf.h"

void listCity() {
	cout << "City list is: \n";
	cout << "0. Alba Iulia\n";
	cout << "1. Brasov\n";
	cout << "2. Sighisoara\n";
	cout << "3. Sibiu\n";
	cout << "4. Bucuresti\n";
	cout << "5. Buzau\n";
	cout << endl;
}

int main()
{
	Graf orase;
	listCity();
	orase.readList();
	orase.Dijkstra();
	system("pause");
	return 0;
}